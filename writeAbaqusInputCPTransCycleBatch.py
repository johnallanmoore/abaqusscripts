#!/usr/bin/python
import numpy as np
from random import *

import sys

def getElsetsMat2(vf,numeElsets,elemInGrains):
    # determine volume fraction of each grain to vary volume fraction.
    volFrac = np.zeros(numElsets)
    for i in range(numElsets):
        elemInThisGrain = elemInGrains[i]
        volFrac[i] = elemInThisGrain[1]/numElem

        #shuffled volume fraction
        volFracShuffle = volFrac[shuffleElsets-1]

        vfTotal = 0.0
        for i in range(len(volFracShuffle)):
            if vfTotal >= vf:
                # length of vfShuffle to include in material 2
                mat2Ind = i
                break
            else:
                vfTotal = vfTotal + volFracShuffle[i]

    print 'volume fraction ' + str(vfTotal)
    elsetsMat2 = shuffleElsets[0:mat2Ind]
    elsetsMat2 = np.sort(elsetsMat2)
    return elsetsMat2
try:
    vfm = sys.argv[1] #  var1
    sampleNum = sys.argv[2] #  var2

except:
    pass
###### files ######
fileName = 'rndCpTransElem64kCycle2'
# mesh files from getNeperNodesElems.py
# node file name
nodeFile = 'neperNodes-tfip.inp'
# element file name
elementFile = 'neperElements-tfip.inp'
# elset file name (with cards)
elsetFile = 'neperElsets-tfip.inp'
# nset file name (with cards)
nsetFile = 'neperNsets-tfip.inp'
# number of elsets
numElsetsFile = 'neperNumElsets-tfip.inp'
# number of elements in each grain
elemInGrainsFile = 'elemInGrains-tfip.inp'
# shuffled grain (ie elset) numbers
shuffleFile = 'shuffle100.inp'

#Orientation file (used the old ones from the parafip study)
orientFile = './parFipEulers/textureRnd118Euler_' + str(sampleNum) + '.txt'

##### user options #######
# element type
elementType = 'C3D8R'
# material name
materialName1 = 'Material-1'
materialName2 = 'Material-2'
# section value
sectionValue = ''
# volume fraction
vf = 0.1

# statevaribales start and end values
sdvStartEnd = [[1,81],[110,133],[202,210]]
numPerLine = 5

# number of cycles (1 = 0.5 cycles, 2 = 1, etc)
numCycle = 11

# mean strain/displacemnt
dispMean = 0.01

# strain/displacment amplitude
dispAmp = 0.023
dispMax = dispMean + 0.5*dispAmp
dispMin = dispMean - 0.5*dispAmp

#output frequency
freq1 = 5 # first cycle
freq2 = 5 # subsiquent cycles



## Create Output file
fileOutput = open(fileName + '.inp','w') 

## Open input files
nodes = np.loadtxt(nodeFile,delimiter=',')
elements = np.loadtxt(elementFile,delimiter=',',dtype=int)
elsetObject = open(elsetFile,'r')
nsetObject =  open(nsetFile,'r')
elsetText = elsetObject.read()
nsetText = nsetObject.read()
numElsets = np.loadtxt(numElsetsFile,dtype=int)
elemInGrains = np.loadtxt(elemInGrainsFile,delimiter=',')
shuffleElsets = np.loadtxt(shuffleFile,dtype=int)

orient = np.loadtxt(orientFile,delimiter=',')

#close up files where data is already extracted
elsetObject.close()
nsetObject.close()
############################################
################ Header ###################  
###########################################
fileOutput.write("""*Heading
*Preprint, echo=NO, model=NO, history=NO, contact=NO
""")

############################################
################## Part  ###################
############################################
fileOutput.write("""**
** PARTS
** \n""")
fileOutput.write('*Part, name=Part-1'+'\n' )
# NODES
fileOutput.write('*Node' +'\n' )
s = nodes.shape
for i in range(s[0]):
    for j in range(s[1]):
        if j != 0:
            fileOutput.write('\t'+',')
            fileOutput.write(str(nodes[i,j]))
        else:
            fileOutput.write(str(int(nodes[i,j])))
    fileOutput.write('\n')

# ELEMENTS
fileOutput.write('*Element, type=' + elementType + '\n' )
s = elements.shape
for i in range(s[0]):
    for j in range(s[1]):
        if j != 0:
            fileOutput.write('\t'+',')
        fileOutput.write(str(elements[i,j]))
    fileOutput.write('\n')

# All Element Set
fileOutput.write('*ELSET, elset=Set-All, generate' + '\n' )
fileOutput.write(str(min(elements[0:s[0],0]))+',')
fileOutput.write(str(max(elements[0:s[0],0]))+'\n')
# assuming no skipped numbers
numElem = float(max(elements[0:s[0],0]))

################ Grain Element Sets (in part) ###################  
fileOutput.write(elsetText + '\n')
fileOutput.write("""
*Elset, elset=nlFip900
59073, 59074, 59075, 59076, 59077, 59113, 59114, 59115, 59116, 59117, 59153, 59154, 59155, 59156, 59157, 59193, 
59194, 59195, 59196, 59197, 60673, 60674, 60675, 60676, 60677, 60713, 60714, 60715, 60716, 60717, 60753, 
60754, 60755, 60756, 60757, 60793, 60794, 60795, 60796, 60797, 62273, 62274, 62275, 62276, 62277, 62313, 
62314, 62315, 62316, 62317, 62353, 62354, 62355, 62356, 62357, 62393, 62394, 62395, 62396, 62397, 63873, 
63874, 63875, 63876, 63877, 63913, 63914, 63915, 63916, 63917, 63953, 63954, 63955, 63956, 63957, 63993, 
63994, 63995, 63996, 63997 
"""+ '\n')
################ Element sets for Each Material ###################  
elemMat1File = 'elemMat1.inp'
elemMat2File = 'elemMat2.inp'
firstElset = True
firstElsetMat1 = True
firstElsetMat2 = True
elsetsMat2 = getElsetsMat2(vf,numElsets,elemInGrains)
counterElset = 1
counterMat2 = 1
fileInput = open(elsetFile,'r')
for line in fileInput:
    words = line.split()
    try:
        if fMat1.closed == False:
            fMat1.write(line)
    except:
        pass
    try:
        if fMat2.closed == False:
            fMat2.write(line)
    except:
        pass
    if words:
        if words[0] == '*Elset,':
            if firstElset == True:
                # need a first mat1 and first mat2 elset
                if counterElset == elsetsMat2[counterMat2-1]:
                    fMat2 = open(elemMat2File,'w')
                    counterMat2 = counterMat2 + 1
                    firstElsetMat2 = False
                else:
                    print 'first elset'
                    fMat1 = open(elemMat1File,'w')
                    firstElsetMat1 = False
                firstElset = False
            else:
                if counterElset == elsetsMat2[counterMat2-1]:
                    if firstElsetMat2 == True:
                        fMat2 = open(elemMat2File,'w')
                        firstElsetMat2 = False
                    else:
                        fMat2 = open(elemMat2File,'a')
                    counterMat2 = counterMat2 + 1
                else:
                    if firstElsetMat1 == True:
                        fMat1 = open(elemMat1File,'w')
                        firstElsetMat1 = False
                    else:
                        fMat1 = open(elemMat1File,'a')

            if counterMat2 >= len(elsetsMat2):
                counterMat2 = len(elsetsMat2) - 1
            counterElset = counterElset + 1
    else:
        try:
            fMat1.close()
        except:
            pass
        try:
            fMat2.close()
        except:
            pass
fileInput.close()

# Section (Need to put elemet sets in part, and remove instance for name)
fileOutput.write('** Section: Section-1' + '\n')
for i in range(numElsets):
    fileOutput.write('*Solid Section, elset=poly' + str(i+1) +  ', material=' + materialName1  + '-' + str(i+1) + '\n')
    fileOutput.write(sectionValue + ',' + '\n')
############################################
################ Hourglass Stiffness ####### 
###########################################
    fileOutput.write('*Hourglass Stiffness' + '\n')
    fileOutput.write('10., , 0., 0.' + '\n')
fileOutput.write('*End Part')
############################################
################ Assembly ###################  
###########################################
fileOutput.write("""
**  
**
** ASSEMBLY
**
*Assembly, name=Assembly
**  
*Instance, name=Part-1-1, part=Part-1
*End Instance""")
fileOutput.write('\n')
######### The rest of this stuff will probably be cut and paste from abaqus
################ Element Sets (after instance) ###################
# open element sets created earlier
elsetObjectMat1 = open(elemMat1File,'r')
elsetObjectMat2 = open(elemMat2File,'r')
elsetMat1 = elsetObjectMat1.read()
elsetMat2 = elsetObjectMat2.read()
# write cards for each material
fileOutput.write('*ELSET, elset='+ materialName1 +', internal, instance=Part-1-1' + '\n')
fileOutput.write(elsetMat1 + '\n')
if len(elsetsMat2) > 0:
    fileOutput.write('*ELSET, elset='+ materialName2 +', internal, instance=Part-1-1' + '\n')
    fileOutput.write(elsetMat2 + '\n')
# write the card for each elset
# write each eset  
# close each files
#fileOutput.write(elsetText + '\n')
################ Node Sets ###################
s = nodes.shape
refNode = int(nodes[s[0]-1,0])
fileOutput.write('*Nset, nset=refNode, internal, instance=Part-1-1' + '\n')
fileOutput.write(str(refNode) + '\n') 
#fileOutput.write(nsetText + '\n')

fileInput = open(nsetFile,'r')
nodeRefFlag = 0
for line in fileInput:
    words = line.split()
    if words:
        if len(words) >= 2:
            if words[1] == 'nset=y1,' or words[1] == 'nset=z1,':
                nodeRefFlag = 1
    if nodeRefFlag == 1:
        for w in range(len(words)):
            if words[w] == str(refNode):
                nodeRefFlag = 0
                fileOutput.write('\n')
                print 'found refnode' + str(words[w])
            else:
                fileOutput.write(words[w] + ' ')
        fileOutput.write('\n')
    else:
        fileOutput.write(line + '\n')
fileInput.close()
fileOutput.write('\n')
################ Equations ###################
fileOutput.write('''** Constraint: Constraint-1
*Equation
2
y1, 2, 1.
refNode, 2, -1.
** 
** Constraint: Constraint-2
*Equation
2
z1, 3, 1.
refNode, 3, -1.
** \n''')
fileOutput.write('*End Assembly \n')
 
############################################
################ Materials ###################  
###########################################

# is there a way for the section properteis to give orientation?
# then I would only need 2 materials and it reads section prop
# use *ORIENTATION and ORIENT subroutine 
fileOutput.write("""** 
** MATERIALS
** 
""")

materialProps1 = '75000,0.3'
materialProps2 = '200000,0.33'

youngs1 = 75000.0

for i in range(numElsets):
    fileOutput.write('*Material, name='+ materialName1 + '-' + str(i+1) + '\n')
    # this is the material inoput Sivom gave us
#     fileOutput.write('''*DEPVAR
# 200,
# *User Material, constants=27, unsymm
# 130000.,  98000.,  34000.,  65000.,  49000.,  17000., 1.1e-05, 7.6e-0
# 8.5,  0.1308,    257.,    277.,    120., 130000.,  98000.,  34000.
# 0.002,    0.02,    310.,   1250.,    900.,     0.1,     1.4,    277.'''+ '\n')
    fileOutput.write('''*DEPVAR
210,
*User Material, constants=27, unsymm
130000.,  98000.,  34000.,  65000.,  49000.,  17000., 1.1e-05, 6.6e-06
     8.4,  0.1308,    257.,    277.,    130., 130000.,  98000.,  34000.
   0.002,    0.02,    320.,   500,    900.,     0.125,     1.4,    277.'''+ '\n')
    fileOutput.write(str(orient[i,0]) + ',')
    fileOutput.write(str(orient[i,1]) + ',' + str(orient[i,2]) + ',')
    fileOutput.write( '\n')
    fileOutput.write('** ----------------------------------------------' + '\n')
#     fileOutput.write('''*DEPVAR
#      200,
# *User Material, constants=27, unsymm
# 12000.,  12000.,  12000.,  12000.,  12000.,  12000., 1.1e-05, 7.6e-06
#      9,  0.1308,    230.,    275.,   130., 12000.,  12000.,  12000.
#    0.002,    0.02,    450.,   1000.,    900.,     0.125,    1.4,    275.'''+ '\n')
#     fileOutput.write(str(orient[i,0]) + ',')
#     fileOutput.write(str(orient[i,1]) + ',' + str(orient[i,2]) + ',')
#     fileOutput.write( '\n')
#     fileOutput.write('** ----------------------------------------------' + '\n')

############################################
################ Step 1 ###################  
###########################################
for s in range(numCycle):
    fileOutput.write('** \n')
    fileOutput.write('** STEP: Step-' + str(s+1)+'\n')
    fileOutput.write('** \n')
    fileOutput.write('*Step, name=Step-'+ str(s+1)+', nlgeom=YES, inc=100000 \n')
    fileOutput.write("""*Static
0.01, 1., 1e-05, 0.1
** 
** 
** 
** BOUNDARY CONDITIONS
** 
*BOUNDARY, type=DISPLACEMENT                          
x0, 1, 1 ,0.0 \n""")
    if s%2 == 0:
        disp = dispMax
    else:
        disp = dispMin
    fileOutput.write('*BOUNDARY, type=DISPLACEMENT \n')                        
    fileOutput.write('x1, 1, 1 ,' + str(disp) + '\n')  
    fileOutput.write("""*BOUNDARY, type=DISPLACEMENT                          
y0, 2, 2 ,0.0 
*BOUNDARY, type=DISPLACEMENT                          
z0, 3, 3 ,0.0  
** 
** CONTROLS
** 
*Controls, reset
*Controls, parameters=field, field=displacement
0.9, 0.9, , , 0.9, , ,   
*Controls, parameters=field, field=hydrostatic fluid pressure
0.9, 0.9, , , 0.9, , , 
*Controls, parameters=field, field=rotation
0.9, 0.9, , , 0.9, , , 
*Controls, parameters=field, field=electrical potential
0.9, 0.9, , , 0.9, , , 
** 
** SOLVER CONTROLS
** 
*Solver Controls, reset
*Solver Controls
  0.8,   
*** \n""")
########### output requests #######
    fileOutput.write("""** 
** OUTPUT REQUESTS
** 
*Restart, write, number interval=1, time marks=NO
** 
** FIELD OUTPUT: F-Output-1
** \n""")
    if s == 0:
        fileOutput.write('*Output, field, variable=PRESELECT, FREQUENCY= ' + str(freq1) + '\n' )
    else:
        fileOutput.write('*Output, field, variable=PRESELECT, FREQUENCY= ' + str(freq2) + '\n' )
    fileOutput.write("""*Element Output
EVOL, """)
    skipline = False
    for i in range(len(sdvStartEnd)):
        a = sdvStartEnd[i]
        sdvStart = a[0]
        sdvEnd = a[1] + 1
        if not skipline:
            fileOutput.write('\n')
            for j in range(sdvStart,sdvEnd):
                skipline = False
                fileOutput.write('SDV'+ str(int(j)));
        
                if i == len(sdvStartEnd)-1 and j == sdvEnd - 1:
                    pass
                else:
                    fileOutput.write(', ')

                if j % int(numPerLine) == 0:
                    fileOutput.write('\n')
                    skipline = True
    
##    fileOutput.write("""
##** 
##** HISTORY OUTPUT: H-Output-1
##**  \n""")

## end step
    fileOutput.write('*End Step \n')
#elsetObject.close()
#nsetObject.close()
fileOutput.close()
