import math
import datetime
from odbAccess import *
from abaqusConstants import *

# Input and ouput file names
InputName='Job-Z'
#OutName='LE'
#OutName='PE'
OutName='S'
#tensor component Tij = [T11, T22, T33, T12, T13, T23 ]
#tensor component Ti  = [T(1), T(2), T(3), T(4), T(5), T(6) ]
tensComp = 3
print 'ODB = ' + InputName
print 'Output Variable = ' + OutName
outputfilename=InputName+'-'+str(OutName)+'-'+ str(tensComp)+'.txt'

## open txt file to write to
out = open(outputfilename,'w')
# opend .dob file to read from
odb = openOdb(InputName+'.odb')
# number of frames in step
numberFrame = len(odb.steps['Step-1'].frames)

# extract frame 1 to determine how many elements it has
frame = odb.steps['Step-1'].frames[1]
outvar=frame.fieldOutputs[str(OutName)]

numelem = len(outvar.values)

# loop over all times (ie frames)
for ns in range (1,numberFrame):
	# these just let you know its running and how long it takes
	print ns
	print str(datetime.datetime.now())

	# extract varibles desired
	frame = odb.steps['Step-1'].frames[ns]
	outvar=frame.fieldOutputs[str(OutName)]
        
        #print outevol.values[1].data
	# average over all elements 
	sumvar=0
	
	for e in range (0,numelem):
		# sum of varible for all elements
		var=outvar.values[e].data[tensComp-1]
            
		sumvar += var

	# average of varibale for all elements	
	avevar = sumvar/numelem
	# write data
	out.write(str(avevar)+'\n')

# close input and output files
out.close()
odb.close()


