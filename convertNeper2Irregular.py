#!/usr/bin/python
import numpy as np
import datetime

def getElemInd(nodes,elements,extrema, extensionIrr): 

    xMin = extrema[0]
    xMax = extrema[1]
    yMin = extrema[2]
    yMax = extrema[3]
    zMin = extrema[4]
    zMax = extrema[5]

    tol = 1e-4
    #tol = 0.013
    
    nodeFile = 'elsetNodes' + extensionIrr+'.inp'
    elementFile = 'elsetElements' + extensionIrr  +'.inp'
    ## Open input files
    nodesIrr = np.loadtxt(nodeFile,delimiter=',')
    elementsIrr = np.loadtxt(elementFile,delimiter=',',dtype=int)

    # shift to match neper nodes which all a position 0 to 1
    #nodes[:,1:4] = nodes[:,1:4] + shift

    # assumes node numbers start at 1 with no breaks
    #if nodeRange == True:
    #    nodes = nodes[startNode-1 : endNode,:]
    #if elementRange == True:
    #    elements = elements[startElement-1 : endElement,:]

    numNodes = nodes.shape[0]
    #print 'Number of Nodes: ' + str(numNodes)

    numElements = elements.shape[0]
    nodesPerElem = elements.shape[1] - 1
    #print 'Number of Elements: ' + str(numElements)
    #print 'Number of Nodes per  Elements: ' + str(nodesPerElem)

    nodeInd = np.empty((numNodes,))
    nodeInd[:] = np.nan

    elemInd = np.empty((numElements,))
    elemInd[:] = np.nan

    #print 'start searching nodes           : ' + str(datetime.datetime.now())

    counter = 0
    for i in range(numNodes):
        xVal = nodes[i,1]
        yVal = nodes[i,2]
        zVal = nodes[i,3]
        # all nodes must be in box
        if (xMin - tol) <=  xVal  and xVal <= (xMax + tol):
            if (yMin - tol) <=  yVal and yVal <= (yMax + tol):
                if (zMin - tol) <=  zVal and zVal <= (zMax + tol):
                    nodeInd[counter] = nodes[i,0]
                    counter = counter + 1

    nodeInd = nodeInd[~np.isnan(nodeInd)]

    #print 'end searching nodes             : ' + str(datetime.datetime.now())
    #print 'start sorting nodes             : ' + str(datetime.datetime.now())
    nodeInd.sort()
    #print 'end sorting nodes               : ' + str(datetime.datetime.now())

    #print 'start searching elements        : ' + str(datetime.datetime.now())
    counter = 0
    for e in range(numElements):
        eElement = elements[e,1:nodesPerElem]
        for i in range(nodesPerElem-1):
            # only one node in range found
            keepElement = False
            if eElement[i] in nodeInd:
                keepElement = True
                #only one node in in range found
                break
#            else:
#                keepElement = False
#                break

        if keepElement:
            elemInd[counter] = elements[e,0]
            counter = counter + 1

    elemInd = elemInd[~np.isnan(elemInd)]
    elemInd = elemInd.astype(np.int64)

    #print 'end searching elements          : ' + str(datetime.datetime.now())
    
    return elemInd


################################################################
################# start main Program ##########################
###############################################################

#extension = '-fip'
extension = '-apsCoarse'

nodeFile = 'neperNodes' + extension + '.inp' 
elementFile = 'neperElements' + extension + '.inp'
elsetFile = 'neperElsets' + extension + '.inp'
nsetFile = 'neperNsets' + extension + '.inp'
numNsetsFile = 'neperNumNsets' + extension + '.inp'
numElsetsFile = 'neperNumElsets' + extension + '.inp'
elemInGrainsFile = 'elemInGrains' + extension + '.inp'


## Open input files
nodes = np.loadtxt(nodeFile,delimiter=',')
elements = np.loadtxt(elementFile,delimiter=',',dtype=int)
elsetObject = open(elsetFile,'r')
nsetObject =  open(nsetFile,'r')
elsetText = elsetObject.read()
nsetText = nsetObject.read()
numElsets = np.loadtxt(numElsetsFile,dtype=int)
elemInGrains = np.loadtxt(elemInGrainsFile,delimiter=',')

elsetMatCol = int(numElsets)
elsetMatRow =  int(max(elemInGrains[:,1]))
elsetMat = np.zeros((elsetMatRow,elsetMatCol),dtype=int)

#close up files where data is already extracted
elsetObject.close()
nsetObject.close()

fileInput = open(elsetFile,'r')

####################################################################
############## Make an Array of elemenst for each elset #############
#####################################################################

grainCounter = -1
for line in fileInput:
    words = line.split()

    if words:
        if words[0] == '*Elset,':
            grainCounter += 1
            elemCounter = 0
        else:
            for i in range(0,len(words)):
                word = words[i]
                if word[-1] == ',':
                    elsetMat[elemCounter,grainCounter]= int(word[0:-1])
                else:
                    elsetMat[elemCounter,grainCounter]= int(word)


                elemCounter += 1 
    
####################################################################
############## Find Elements in Irregularr Mesh #############
#####################################################################
# True if nodes or elements are limited to a range
# givine my startNode/Element endNode/Element
nodeRange = False
elementRange = False
useOnlyMatrix = False

startNode = 1
endNode = 12

startElement = 1
endElement = 2

###############################
######   LOAD FILES   #########
###############################

# irregular finite element mesh name
#deckNameIrr = 'fullInclusion'
deckNameIrr = 'fineMeshTet'

#extensionIrr = '-irr'
extensionIrr = '-apsirrt'

#abaqus output files for irreglar mesh
elsetFileIrr = deckNameIrr + '-Elsets.inp'
fileOutput = open(elsetFileIrr ,'w')

# node file name (from getElsetNodesElems.py)
nodeFileIrr = 'elsetNodes'+ extensionIrr + '.inp'
# element file name
elementFileIrr = 'elsetElements' + extensionIrr + '.inp'

nodesIrr = np.loadtxt(nodeFileIrr,delimiter=',')
elementsIrr = np.loadtxt(elementFileIrr,delimiter=',',dtype=int)

# matrix elements
if useOnlyMatrix:
    matrixElementsFileIrr = 'matrixElements-fip.inp'
    matrixElements = np.loadtxt(matrixElementsFileIrr,delimiter=',',dtype=int)
    matrixElements = matrixElements.flatten()
    matrixElementsInd = matrixElements - 1
    elementsIrr = elementsIrr[matrixElementsInd]

    # matrix nodes
    matrixNodesFileIrr = 'matrixNodes-fip.inp'
    matrixNodes = np.genfromtxt(matrixNodesFileIrr,delimiter=',',dtype=int, filling_values=0)
    matrixNodes = matrixNodes.flatten()
    matrixNodes = matrixNodes[np.nonzero(matrixNodes)]
    matrixNodesInd = matrixNodes - 1
    nodesIrr = nodesIrr[matrixNodesInd]

# shift in irregular mesh to match neper 0 to 1 cell
#shift = 0.5
shift = 0.0

# shift to match neper nodes which all a position 0 to 1
nodesIrrShift = nodesIrr
nodesIrrShift[:,1:4] = nodesIrrShift[:,1:4] + shift

elemIndMatArray = []
grainList = np.loadtxt('grainList.txt',dtype=int)

for grain in grainList:
#for grain in range(elsetMatCol):
#for grain in range(4):
    #grain = 0
    grain = grain - 1
    
    print 'start grain  : ' +str(grain+1) + ' ' + str(datetime.datetime.now())
    elemRange = int(elemInGrains[grain,1])
    # elemRange = 2
    # padding so all element idices can fit 
    # if working with a huge irregular mesh or a very coarse neper mesh
    #  this might need to be higher
    padding = 1000

    elemIndMat =  np.empty((elemRange,padding,))
    elemIndMat[:] = np.nan
    
    for e in range(elemRange):

        elsetInd = elsetMat[e,grain]-1
        elem =  elements[elsetInd,:]

        nodesInElem = elements[elsetInd,1:len(elem)]

        nodeCoordRow =  len(nodesInElem)
        nodeCoordCol = len(nodes[0,:]) - 1
        nodeCoord = np.zeros((nodeCoordRow,nodeCoordCol))

        #print nodeCoord
        for n, node in enumerate(nodesInElem):
            nodeInd = node-1
            nodeCoord[n,:] =  nodes[nodeInd,1:nodeCoordCol+1]

        #print nodeCoord
        xMin = min(nodeCoord[:,0])
        xMax = max(nodeCoord[:,0])
        yMin = min(nodeCoord[:,1])
        yMax = max(nodeCoord[:,1])
        zMin = min(nodeCoord[:,2])
        zMax = max(nodeCoord[:,2])



        extrema = [xMin, xMax, yMin, yMax, zMin, zMax]

        #####################################################################
        ###################### find elements in irregular mesh ##############
        #####################################################################
        
        elemInd = getElemInd(nodesIrrShift,elementsIrr, extrema, extensionIrr)
        

        if len(elemInd) != 0:
            elemIndMat[e,0:len(elemInd)] = elemInd

    elemIndMatArray.append(elemIndMat)

    print 'end grain  : ' + str(datetime.datetime.now())


for grain in range(len(grainList)):
#for grain in range(elsetMatCol):
#for grain in range(4):
     
    elemIndMat = elemIndMatArray[grain]
    
    # element Set name
    #elsetName = 'poly' + str(grain + 1)
    elsetName = 'poly' + str(grainList[grain])

    print 'start writing elements to file  : ' + str(datetime.datetime.now())
    fileOutput.write('\n *Elset, elset=' + elsetName + '\n')

    counter = 0


    for i in range(len(elemIndMat)):
        for col in range(padding):
            elemIndValue = elemIndMat[i,col]
            if np.isnan(elemIndValue) == False:
                fileOutput.write(str(int(elemIndValue)) + ', ')
                counter += 1
            else:
                break
            if  counter % 15 == 0:
                fileOutput.write('\n')        
    print 'end writing elements to file    : ' + str(datetime.datetime.now())                         
fileOutput.close()
