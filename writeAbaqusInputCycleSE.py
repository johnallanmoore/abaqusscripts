#!/usr/bin/python
import numpy as np
from random import *



###### files ######
#.inp file name
#fileName = 'vfExampleMesh'
fileName = 'singleElemCycleU'

##### user options #######
# number of cycles (1 = 0.5 cycles, 2 = 1, etc)
numCycle = 7
# displacment value in x
dispMax = 0.22
dispMin = -0.32
## Create Output file
fileOutput = open(fileName + '.inp','w') 

############################################
################ Header ###################  
###########################################
fileOutput.write("""*Heading
*Preprint, echo=NO, model=NO, history=NO, contact=NO
**
** PARTS
** 
*Part, name=Part-1
*Node
      1,           1.,           1.,           1.
      2,           1.,           0.,           1.
      3,           1.,           1.,           0.
      4,           1.,           0.,           0.
      5,           0.,           1.,           1.
      6,           0.,           0.,           1.
      7,           0.,           1.,           0.
      8,           0.,           0.,           0.
*Element, type=C3D8
1, 5, 6, 8, 7, 1, 2, 4, 3
*ELSET, elset=Set-All, generate
1,1,1
*Elset, elset=poly1
1
** Section: Section-1
*Solid Section, elset=poly1, material=Material-1-1
,
*Hourglass Stiffness
10., , 0., 0.
*End Part
**  
**
** ASSEMBLY
**
*Assembly, name=Assembly
**  
*Instance, name=Part-1-1, part=Part-1
*End Instance
**
*Nset, nset=Set-1, instance=Part-1-1, generate
 5,  8,  1
*Elset, elset=Set-1, instance=Part-1-1
 1,
*Nset, nset=Set-2, instance=Part-1-1, generate
 2,  8,  2
*Elset, elset=Set-2, instance=Part-1-1
 1,
*Nset, nset=Set-3, instance=Part-1-1
 3, 4, 7, 8
*Elset, elset=Set-3, instance=Part-1-1
 1,
*Nset, nset=x0, internal, instance=Part-1-1, generate
 5,  8,  1
*Nset, nset=x1, internal, instance=Part-1-1, generate
 1,  4,  1
*Nset, nset=y0, internal, instance=Part-1-1
 2,    4,  6, 8
*Nset, nset=y1, internal, instance=Part-1-1
 1,    3,  5, 7
*Nset, nset=z0, internal, instance=Part-1-1
 3,4,7,8
*Nset, nset=z1, internal, instance=Part-1-1
 1,2,5,6
*Elset, elset=_PickedSet7, internal, instance=Part-1-1
 1,
*Nset, nset=_PickedSet8, internal, instance=Part-1-1, generate
 2,  8,  2
*Elset, elset=_PickedSet8, internal, instance=Part-1-1
 1,
*Nset, nset=_PickedSet9, internal, instance=Part-1-1
 3, 4, 7, 8
*Elset, elset=_PickedSet9, internal, instance=Part-1-1
 1,
*Nset, nset=_PickedSet10, internal, instance=Part-1-1, generate
 1,  4,  1
*Elset, elset=_PickedSet10, internal, instance=Part-1-1
 1,
** Constraint: Constraint-1
** *Equation
** 2
** y1, 2, 1.
** refNode, 2, -1.
** ** 
** ** Constraint: Constraint-2
** *Equation
** 2
** z1, 3, 1.
** refNode, 3, -1.
** ** 
*End Assembly 
** 
** MATERIALS
** 
*Material, name=Material-1-1
*Elastic, type=iso
15e3, 0.3
** *Plastic
** 400.0, 0.0
** 600.0, 1.0
** First line
** Young's modulus of martensite, 
** Poisson's ratio of martensite, 
** Uniaxial transformation strain, 
** Stress at which the transformation begins during loading in tension, 
** Stress at which the transformation ends during loading in tension, 
** Stress at which the reverse transformation begins during unloading in tension, 
** Stress at which the reverse transformation ends during unloading in tension, 
** Stress at which the transformation begins during loading in compression, as a positive value, 
** Second line
** Reference temperature, 
** Slope of the stress versus temperature curve for loading, 
** Slope of the stress versus temperature curve for unloading, 
*SUPERELASTIC
15e3,0.3,0.1, 600, 745, 360, 135, 250
300,0.0,0.0
*SUPERELASTIC HARDENING
1200., 0.16
3300., 1.0
** ----------------------------------------------
** ----------------------------------------------\n""")
for i in range(numCycle):
    fileOutput.write('** \n')
    fileOutput.write('** STEP: Step-' + str(i+1)+'\n')
    fileOutput.write('** \n')
    fileOutput.write('*Step, name=Step-'+ str(i+1)+', nlgeom=YES, inc=100000 \n')
    fileOutput.write("""*Static
0.005, 1., 1e-05, 0.01
** 
** 
** BOUNDARY CONDITIONS
** 
*BOUNDARY, type=DISPLACEMENT                          
x0, 1, 1 ,0.0 \n""")
    if i%2 == 0:
        disp = dispMax
    else:
        disp = dispMin
    fileOutput.write('*BOUNDARY, type=DISPLACEMENT \n')                        
    fileOutput.write('x1, 1, 1 ,' + str(disp) + '\n')  
    fileOutput.write("""*BOUNDARY, type=DISPLACEMENT                          
y0, 2, 2 ,0.0 
*BOUNDARY, type=DISPLACEMENT                          
z0, 3, 3 ,0.0  
** 
** OUTPUT REQUESTS
** 
*Restart, write, number interval=1, time marks=NO
** 
** FIELD OUTPUT: F-Output-1
** 
*Output, field, variable=PRESELECT
*Element Output
EVOL,TE, TEEQ, TEVOL, MVF
** 
** HISTORY OUTPUT: H-Output-1
**  
* end step \n""")

fileOutput.close()
