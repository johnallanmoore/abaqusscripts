#!/usr/bin/python
import numpy as np

#.inp file name
fileName = 'testfile'
# node file name
nodeFile = 'testNodes.inp'
# element file name
elementFile = 'testElements.inp'
# element type
elementType = 'T2D2'
# material name
materialName = 'Material-1'
# section value
sectionValue = 1.0

## Create Output files files
fileOutput = open(fileName + '.inp','w') 

## Open input files
nodes = np.loadtxt(nodeFile,delimiter=',')
elements = np.loadtxt(elementFile,delimiter=',',dtype=int)

############################################
################ Header ###################  
###########################################
fileOutput.write("""*Heading
*Preprint, echo=NO, model=NO, history=NO, contact=NO
""")

############################################
################## Part  ###################
############################################
fileOutput.write("""**
** PARTS
** \n""")
fileOutput.write('*Part, name=Part-1'+'\n' )
# NODES
fileOutput.write('*Node' +'\n' )
s = nodes.shape
for i in range(s[0]):
    for j in range(s[1]):
        if j != 0:
            fileOutput.write('\t'+',')
            fileOutput.write(str(nodes[i,j]))
        else:
            fileOutput.write(str(int(nodes[i,j])))
    fileOutput.write('\n')

# ELEMENTS
fileOutput.write('*Element, type=' + elementType + '\n' )
s = elements.shape
for i in range(s[0]):
    for j in range(s[1]):
        if j != 0:
            fileOutput.write('\t'+',')
        fileOutput.write(str(elements[i,j]))
    fileOutput.write('\n')

# All Element Set
fileOutput.write('*ELSET, elset=Set-All, generate' + '\n' )
#s = elements.shape
#for i in range(s[0]):
#    fileOutput.write(str(elements[i,0]))
#    fileOutput.write('\n')
fileOutput.write(str(min(elements[0:s[0],0]))+',')
fileOutput.write(str(max(elements[0:s[0],0]))+'\n')

# Section
fileOutput.write('** Section: Section-1' + '\n')
fileOutput.write('*Solid Section, elset=Set-All, material='+materialName + '\n')
fileOutput.write(str(sectionValue) + ',' + '\n')

fileOutput.write('*End Part')
############################################
################ Assembly ###################  
###########################################
fileOutput.write("""
**  
**
** ASSEMBLY
**
*Assembly, name=Assembly
**  
*Instance, name=Part-1-1, part=Part-1
*End Instance""")
fileOutput.write('\n')
######### The rest of this stuff will probably be cut and paste from abaqus
################ Node Sets ###################  
fileOutput.write("""*NSET, nset=leftNode,  internal, instance=Part-1-1
3
*NSET, nset=rightNode,  internal, instance=Part-1-1
2
*NSET, nset=bottomNode,  internal, instance=Part-1-1
1
*ELSET, elset=rightElement, internal, instance=Part-1-1
1
*ELSET, elset=topElement, internal, instance=Part-1-1
2
*ELSET, elset=leftElement, internal, instance=Part-1-1
3
**  
*End Assembly \n""")
 
############################################
################ Materials ###################  
###########################################
fileOutput.write("""** 
** MATERIALS
** 
""")
fileOutput.write('*Material, name='+materialName + '\n')
fileOutput.write("""*Elastic
75000,0.3
** ------------------------------------------------""")

############################################
################ Step 1 ###################  
###########################################
fileOutput.write("""** 
** STEP: Step-1
** 
*Step, name=Step-1, nlgeom=NO
*Static
** 
""")

########### Boundary Conditions #######
fileOutput.write("""** 
** BOUNDARY CONDITIONS
** 
*BOUNDARY, type=DISPLACEMENT                          
LEFTNODE, 1, 2 ,0.0 
*BOUNDARY, type=DISPLACEMENT                        
RIGHTNODE, 2, 2 ,0.0
*CLOAD
BOTTOMNODE,2,-1731.0 \n""")

########### output requests #######
fileOutput.write("""** 
** OUTPUT REQUESTS
** 
*Restart, write, number interval=1, time marks=NO
** 
** FIELD OUTPUT: F-Output-1
** 
*Output, field, variable=PRESELECT
*Element Output
LE, PE, PEEQ, S, NFORC
*Node Output
U, RF
** 
** HISTORY OUTPUT: H-Output-1
** 
*Output, history, variable=PRESELECT
*Node print, nset=bottomNode                                
U2,
*Node print, nset=leftNode                                
RF2,
*Node print, nset=rightNode                                
RF2,
*El print, elset=topElement
NFORC
*El print, elset=topElement
S
*El print, elset=leftElement
NFORC
*El print, elset=rightElement
NFORC \n""")

## end step
fileOutput.write('*End Step')

fileOutput.close()
