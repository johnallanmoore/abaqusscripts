#!/usr/bin/python

import numpy as np

import sys

try:
    sampleNum = sys.argv[1] #  var1
    elemNum = sys.argv[2] #  var2
except:
    pass

# elset input file
#elsetInput = 'vfExampleElem1k.inp'
#elsetInput = '/home/moorej/abaqusJobs/elsetPost/elsetPost.inp'
#elsetInput = 'fullInclusion.inp'

#elsetInput = '/home/moorej/neperJobs/parallelFip/parallelFipTess'+ str(sampleNum) +'Elem' + str(elemNum) + 'x.inp'
#elsetInput = 'fineMeshTet.inp'
#elsetInput = 'fineMeshReducedBase.inp'
elsetInput = 'al4.inp'

extensionFlag = False

#extension = '-irr' # irregualr mesh
#extension = '-pfip' # paralized fip mesh
#extension = '-apsirrt' # test aps mesh
extension = '-apsirr' # real aps mesh


if extensionFlag == True:
    #abaqus output files
    nodeFile = 'elsetNodes' + extension + '.inp' 
    elementFile = 'elsetElements' + extension + '.inp'
    elsetFile = 'elsetElsets' + extension + '.inp'
    nsetFile = 'elsetNsets' + extension + '.inp'
    numNsetsFile = 'elsetNumNsets' + extension + '.inp'
    numElsetsFile = 'elsetNumElsets' + extension + '.inp'
else:
    #abaqus output files
    nodeFile = 'elsetNodes.inp'
    elementFile = 'elsetElements.inp'
    elsetFile = 'elsetElsets.inp'
    nsetFile = 'elsetNsets.inp'
    numNsetsFile = 'elsetNumNsets.inp'
    numElsetsFile = 'elsetNumElsets.inp'

# open elemInGrainsFile here some it deletes old stuff
#felemInGrains = open(elemInGrainsFile,'w')

fileInput = open(elsetInput,'r')

counter = 0
counterElset = 0
counterNset = 0
firstElset = True
firstNset = True
firstLine = False
#isElset = False
#elsetElemTotal = 0
for line in fileInput:
    words = line.split()
    if counter < 40000000000:
#         try:
#             if f.closed == False:
#                 #f.write(line+'\n' )
#                 f.write(line )
#                 # count the number of element in a elset line
# #                if words:
# #                    if isElset == True:
# #                        numElemInGrain = numElemInGrain + len(words)
#         except:
#             pass
        # this checks if there is anything in the lines
        if words:
            if words[0] == '*Node':
                print 'its a node'
                firstLine = True
                f = open(nodeFile,'w') 
            elif words[0] == '*Element,':
                print 'its an element'
                firstLine = True
                f = open(elementFile,'w') 
            elif words[0] == '*Elset,':
                firstLine = True
                if firstElset == True:
                    print 'its the first element set'
                    f = open(elsetFile,'w')
                    #isElset = True
                    numElemInGrain = 0
                    firstElset = False
                else:
                    f = open(elsetFile,'a')
                    #isElset = True
                    numElemInGrain = 0

                # write the elset card
                # skip the carriage return and add some Abaaqus stuff
                # use this one if the elsets are define after the istance
                #f.write(line[0:-1] + ', internal, instance=Part-1-1' + '\n' )
                # use this one if the elsets are define in the part)
                f.write(line[0:-1]  + '\n' )
                counterElset = counterElset + 1

            elif words[0] == '*Nset,':
                firstLine = True
                if firstNset == True:
                    print 'its the first node set'
                    f = open(nsetFile,'w')
                    firstNset = False
                else:
                    f = open(nsetFile,'a')

                # write the nset card
                # skip the carriage return and add some Abaaqus stuff
                f.write(line[0:-1] + ', internal, instance=Part-1-1' + '\n' )
                counterNset = counterNset + 1
            elif words[0] == '**':
                try:
                    f.close() 
                except:
                    pass
            elif words[0] == '*Solid':
                f.close() 
            elif words[0] == '*End':
                f.close() 
            elif words[0] == '*Material':
                f.close()  
            elif words[0] == '*Boundary':
                f.close() 
            elif words[0] == '*Step':
                f.close()             
        else:
            try:
                f.close()
            except:
                pass
            # if isElset == True:
            #     felemInGrains.write(str(counterElset) + ',' + str(numElemInGrain) +'\n' )
            #     #print 'elset:' +  str(counterElset) + ' elems:' +  str(numElemInGrain)
            #     elsetElemTotal = elsetElemTotal +  numElemInGrain
            #     isElset = False


        try:
            if f.closed == False:
                #f.write(line+'\n' )
                if firstLine == True:
                    firstLine = False
                else:
                    f.write(line )
                # count the number of element in a elset line
#                if words:
#                    if isElset == True:
#                        numElemInGrain = numElemInGrain + len(words)
        except:
            pass



    counter = counter + 1


# write number of nsets and elsets to files
print 'there are ' + str(counterNset) + ' node sets'
fnset = open(numNsetsFile,'w') 
fnset.write(str(counterNset))
fnset.close()

print 'there are ' + str(counterElset) + ' element sets'
felset = open(numElsetsFile,'w') 
felset.write(str(counterElset))
felset.close()

#print 'there are ' + str(elsetElemTotal) + ' elements'

#felemInGrains.close()
fileInput.close()

