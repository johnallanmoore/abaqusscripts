#!/usr/bin/python

import numpy as np



neperInput = 'grains_L1_C001500_ff_000181MeshFine.inp'
extensionFlag = True
extension = '-apsFine'

if extensionFlag == True:
    #abaqus output files

    elsetFile = 'neperElsets' + extension + '.inp'
    numElsetsFile = 'neperNumElsets' + extension + '.inp'
    elemInGrainsFile = 'elemInGrains' + extension + '.inp'
else:
    #abaqus output files
    elsetFile = 'neperElsets.inp'
    numElsetsFile = 'neperNumElsets.inp'
    elemInGrainsFile = 'elemInGrains.inp'

# open elemInGrainsFile here some it deletes old stuff
elemInGrains = np.loadtxt(elemInGrainsFile,delimiter=',')

maxCol = int(max(elemInGrains[:,1]))
maxRow = int(np.loadtxt(numElsetsFile,delimiter=','))
elsetsMat = np.zeros((maxRow,maxCol))
elsetsMat[:,:] = np.nan

fileInput = open(elsetFile,'r')

outFile  = 'neperElsetsMat' + extension + '.inp'
f = open(outFile,'w')

row = -1

for line in fileInput:
    words = line.split()

    # this checks if there is anything in the lines
    if words:

        if words[0] == '*Elset,':
            row = row + 1
            col = 0
        else:

            for i in range(len(words)):
                w = words[i]
                if w[-1] == ',':
                    value = w[0:-1]
                else:
                    value = w
                elsetsMat[row,col] = value
                col = col + 1
            
for i in range(maxRow):
    for j in range(maxCol):
        if np.isnan(elsetsMat[i,j]):
            storeValue = '0'
        else:
            storeValue = str(int(elsetsMat[i,j]))
        if j == maxCol-1:
            f.write( storeValue + '\n')
        else:
            f.write( storeValue + ', ')


f.close()
