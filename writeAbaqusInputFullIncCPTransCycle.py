#!/usr/bin/python
import numpy as np


###### files ######
#.inp file name to build mesh from
fileName = 'fullInclusionX'


# file with nonlocal element sets from
# createAllNlElsetsInRegionPara.py
nlElsetsFileName = 'elsetAllPost-Elsets.inp'

nlElsets = open(nlElsetsFileName ,'r')


########################################
##### user options #####################
########################################
# element type
elementType = 'C3D8R'
# section value
sectionValue = ''

# statevaribales start and end values
sdvStartEnd = [[1,81],[110,133],[202,210]]
numPerLine = 5

# number of cycles (1 = 0.5 cycles, 2 = 1, etc)
numCycle = 11

# stress concentration factor
Kt = 1.53

# mean strain/displacemnt
dispMean = 0.01
#dispMean = dispMean/Kt 

# strain/displacment amplitude
dispAmp = 0.01
#dispAmp = dispAmp/Kt
dispMax = dispMean + 0.5*dispAmp
dispMin = dispMean - 0.5*dispAmp

#output frequency
freq1 = 5 # first cycle
freq2 = 5 # subsiquent cycles

#Inclusion Properties
Youngs2 = 200e3
Poissons2 = 0.3

# Materials Names
materialNameMatrix = 'Material-1'
materialNameInclusion = 'Material-2'

#Orientation file
orientFile = 'textureRndEuler.txt'
orient = np.loadtxt(orientFile,delimiter=',')

#number of grains
numElsets = 100

## Create Output file
fileOutput = open(fileName + 'CPTransCycle' + '.inp','w') 

################ Element sets for Each Material ###################  
writeSection = False
writeMaterial = False


fileInput = open(fileName +'.inp','r')
for line in fileInput:
    words = line.split()
    if words:
        if words[0] == '*Solid': 
            if writeSection == False:
                writeSection = True
                # first write nonlocal element sets
                for nlLine in nlElsets:
                    fileOutput.write(nlLine)
                fileOutput.write('\n')


                ###### write sections ###################
                fileOutput.write('*Solid Section, elset=INCLUSION, material='+ materialNameInclusion + '\n')
                fileOutput.write(sectionValue + ',' + '\n')
                fileOutput.write('** Section: Section-1' + '\n')
                if elementType == 'C3D8R':
                    fileOutput.write('*Hourglass Stiffness'+ '\n')
                    fileOutput.write('10., , 0., 0.' + '\n')
                for i in range(numElsets):
                    fileOutput.write('*Solid Section, elset=poly' + str(i+1) +  ', material=' + materialNameMatrix + '-' + str(i+1) + '\n')
                    fileOutput.write(sectionValue + ',' + '\n')
                    if elementType == 'C3D8R':
                        fileOutput.write('*Hourglass Stiffness'+ '\n')
                        fileOutput.write('10., , 0., 0.' + '\n')
        elif words[0] == '*End':
            if len(words) > 1:
                if (words[1] == 'Part'):
                    writeSection = False
        elif words[0] == '*Material,':
            ############################################
            ################ Materials ###################  
            ###########################################
            fileOutput.write("""** 
** MATERIALS
** 
""")
            for i in range(numElsets):
                fileOutput.write('*Material, name='+ materialNameMatrix + '-' + str(i+1) + '\n')
    # this is the material inoput Sivom gave us
#     fileOutput.write('''*DEPVAR
# 200,
# *User Material, constants=27, unsymm
# 130000.,  98000.,  34000.,  65000.,  49000.,  17000., 1.1e-05, 7.6e-0
# 8.5,  0.1308,    257.,    277.,    120., 130000.,  98000.,  34000.
# 0.002,    0.02,    310.,   1250.,    900.,     0.1,     1.4,    277.'''+ '\n')
                fileOutput.write('''*DEPVAR
210,
*User Material, constants=27, unsymm
130000.,  98000.,  34000.,  65000.,  49000.,  17000., 1.1e-05, 6.6e-06
     8.4,  0.1308,    257.,    277.,    130., 130000.,  98000.,  34000.
   0.002,    0.02,    320.,   500,    900.,     0.125,     1.4,    277.'''+ '\n')
                fileOutput.write(str(orient[i,0]) + ',')
                fileOutput.write(str(orient[i,1]) + ',' + str(orient[i,2]) + ',')
                fileOutput.write( '\n')
                fileOutput.write('** ----------------------------------------------' + '\n')

            fileOutput.write('*Material, name='+ materialNameInclusion  + '\n')
            fileOutput.write('*Elastic' + '\n')
            fileOutput.write(str(Youngs2) + ',' + str(Poissons2) + '\n')
            break
        
    if writeSection == False:
        if words:
            if words[0] == '*Element,':
                fileOutput.write('*Element, type=' + elementType + '\n')
            else:
                fileOutput.write(line)
        else:
            fileOutput.write(line)

fileInput.close()
 
############################################
################ Step 1 and Boundary ###################  
###########################################
fileOutput.write("""** ----------------------------------------------\n""")
for s in range(numCycle):
    fileOutput.write('** \n')
    fileOutput.write('** STEP: Step-' + str(s+1)+'\n')
    fileOutput.write('** \n')
    fileOutput.write('*Step, name=Step-'+ str(s+1)+', nlgeom=YES, inc=100000 \n')
    fileOutput.write("""*Static
0.01, 1., 1e-05, 0.1
** 
** 
** BOUNDARY CONDITIONS
** 
*BOUNDARY, type=DISPLACEMENT                          
x0, 1, 1 ,0.0 \n""")
    if s%2 == 0:
        disp = dispMax
    else:
        disp = dispMin
    fileOutput.write('*BOUNDARY, type=DISPLACEMENT \n')                        
    fileOutput.write('x1, 1, 1 ,' + str(disp) + '\n')  
    fileOutput.write("""*BOUNDARY, type=DISPLACEMENT                          
y0, 2, 2 ,0.0 
*BOUNDARY, type=DISPLACEMENT                          
z0, 3, 3 ,0.0
**  
** CONTROLS
** 
*Controls, reset
*Controls, parameters=field, field=displacement
0.9, 0.9, , , 0.9, , ,   
*Controls, parameters=field, field=hydrostatic fluid pressure
0.9, 0.9, , , 0.9, , , 
*Controls, parameters=field, field=rotation
0.9, 0.9, , , 0.9, , , 
*Controls, parameters=field, field=electrical potential
0.9, 0.9, , , 0.9, , , 
** 
** SOLVER CONTROLS
** 
*Solver Controls, reset
*Solver Controls
  0.8,   
*** \n""")
########### output requests #######
    fileOutput.write("""** 
** OUTPUT REQUESTS
** 
*Restart, write, number interval=1, time marks=NO
** 
** FIELD OUTPUT: F-Output-1
** \n""")
    if s == 0:
        fileOutput.write('*Output, field, variable=PRESELECT, FREQUENCY= ' + str(freq1) + '\n' )
    else:
        fileOutput.write('*Output, field, variable=PRESELECT, FREQUENCY= ' + str(freq2) + '\n' )
    fileOutput.write("""*Element Output
EVOL, """)
    skipline = False
    for i in range(len(sdvStartEnd)):
        a = sdvStartEnd[i]
        sdvStart = a[0]
        sdvEnd = a[1] + 1
        if not skipline:
            fileOutput.write('\n')
            for j in range(sdvStart,sdvEnd):
                skipline = False
                fileOutput.write('SDV'+ str(int(j)));
        
                if i == len(sdvStartEnd)-1 and j == sdvEnd - 1:
                    pass
                else:
                    fileOutput.write(', ')

                if j % int(numPerLine) == 0:
                    fileOutput.write('\n')
                    skipline = True
    
##    fileOutput.write("""
##** 
##** HISTORY OUTPUT: H-Output-1
##**  \n""")

## end step
    fileOutput.write('*End Step \n')
fileOutput.close()
