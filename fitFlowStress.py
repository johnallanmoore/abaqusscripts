#!/usr/bin/python

import numpy as np
import matplotlib.pyplot as plt
import scipy.optimize as opt

def fitSwift(xdata, *params):
    e0 = params[0]
    ns = params[1]
    yFit = np.zeros(len(xdata))
    for i in range(len(xdata)):
        yFit[i] = sigy*(1.0 + xdata[i]/e0)**ns
    return yFit

############################################################
################     Load Sim Results  #####################
############################################################
# No texture
#deckName = 'vf0Elem64kTi64'
# Ti64 texture in X
#deckName = 'testElem64kTi64'
# Ti64 texture in Z
#deckName = 'testElem64kTi64-Z'
# Ti64 texture in 45
deckName = 'testElem64kTi64-45'

resultsFile = deckName+'-SDV71.txt'
epsb = np.loadtxt(resultsFile)
epsb = np.insert(epsb,0,0.0)

resultsFile = deckName+'-S-Mises.txt'
sige = np.loadtxt(resultsFile)
sige = np.insert(sige,0,0.0)

############################################################
################     Fit Results  #####################
############################################################
sigy = 530.0
for i in range(len(sige)):
    if sige[i] >= sigy :
        searchInd = i
        break

p = opt.curve_fit(fitSwift,epsb[searchInd:-1],sige[searchInd:-1],[1,1])
coef = p[0]
sigbrSwift = fitSwift(epsb,coef[0],coef[1])
print coef

############################################################
################     Plot Results  #####################
############################################################

plt.figure(4)
plt.rcParams.update({'font.size': 16})
plt.plot(epsb[1:-1], sige[1:-1],'k')
plt.plot(epsb, sigbrSwift,'r')
plt.xlabel('Equivalent Strain')
plt.ylabel('von Mises Stress, MPa')
plt.gcf().subplots_adjust(left=0.15)
plt.gcf().subplots_adjust(bottom=0.15)
plt.xlim([0, 0.024])
plt.legend(['Full Simulation','Fit'])

plt.show()
