#!/usr/bin/python

import random 
import numpy as np

# number of seen
numSeeds = 100

# name of ouput file
outFileName = 'shuffleXXX.inp'
f = open(outFileName,'w')

nums = np.arange(1,numSeeds)
np.random.shuffle(nums)

for i in range(numSeeds-1):
    f.write(str(nums[i]) + '\n')

f.close()
