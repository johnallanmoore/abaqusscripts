#!/usr/bin/python

import random 
import numpy as np

# number of seen
numSeeds = 5000

baseName =  'creepRndPct'
propNames = ['Yield','A','N']
stdev = [0.1,0.1,0.1]

for i in range(len(propNames)):

    # name of ouput file
    outFileName = baseName + propNames[i] + '.txt'
    f = open(outFileName,'w')

    nums = np.random.normal(loc=1.0, scale=stdev[i], size=numSeeds)
    np.random.shuffle(nums)

    for i in range(numSeeds-1):
        f.write(str(nums[i]) + '\n')

    f.close()
