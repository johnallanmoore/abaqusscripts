import numpy as np
import os
fileList = ['test1.txt','test2.txt','test3.txt','test4.txt']
outName = 'test.txt'
outFile = open(outName,'w')
for i in range(len(fileList)):
    fileName = fileList[i]
    fileInput = open(fileName,'r')
    if os.stat(fileName).st_size != 0:
        for line in fileInput:
            outFile.write(line)
    else:
        print('mssing file ' + fileName)
    fileInput.close()

outFile.close()
