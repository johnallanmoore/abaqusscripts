#!/usr/bin/python

import matplotlib.pyplot as plt
import numpy as np
from mpl_toolkits.mplot3d import Axes3D
from scipy.optimize import fsolve

def fun(x,*params):
    A = params[0]
    B = params[1]
    N = params[2]
    if A < 1.0 and B < 1.0:
        y = 2.*(A*x + 1)*(B*x + 1)*x - N
    elif A > 1.0 and B > 1.0:
        y = 2.*(A*x - 1)*(B*x - 1)*x - N
    elif A > 1.0 and B == 1.0:
        y = 2.*(A*x - 1)*(B*x)*x - N
    elif B > 1.0 and A == 1.0:
        y = 2.*(A*x)*(B*x - 1)*x - N
    elif A < 1.0 and B > 1.0:
        y = 2.*(A*x + 1)*(B*x - 1)*x - N
    elif A < 1.0 and B == 1.0:
        y = 2.*(A*x + 1)*(B*x)*x - N
    elif B < 1.0 and A > 1.0:
        y = 2.*(A*x - 1)*(B*x + 1)*x - N
    elif B < 1.0 and A == 1.0:
        y = 2.*(A*x)*(B*x + 1)*x - N
    elif A == 1.0 and B == 1.0:
        y = 2.*x**3 - N

    return y


def sphere(x0,y0,z0,R):
    # draw sphere
    u, v = np.mgrid[0:2*np.pi:8j, 0:np.pi:4j]
    #xc = x0*np.ones()
    s = u.shape
    xc = x0*np.ones(s)
    yc = y0*np.ones(s)
    zc = z0*np.ones(s)
    x = xc + R*np.cos(u)*np.sin(v)
    y = yc + R*np.sin(u)*np.sin(v)
    z = zc + R*np.cos(v)
    return x,y,z

# file names
shapeFile = 'foamShape.in'
f = open(shapeFile,'w')
# write the opening stuff
f.write('SHAPE \n') 
f.write('npics 6 \n')                                                                                   


fig = plt.figure()
ax = fig.gca(projection='3d', proj_type = 'ortho')
#ax.set_aspect("equal")
A = 1.0
B = 0.5
vf = 0.7
Lx = 2.
Ly = A*Lx
Lz = B*Lx
a0 = 0.1

VL = Lx*Ly*Lz
Vv = (4./3.)*np.pi*a0**3.0
print "VL " + str(VL)

print "Vv " + str(Vv)


N = vf*VL/Vv
print "N " + str(N)

#Nx = round(N**(1./3.))
Nx = fsolve(fun,round(N**(1./3.)),(A,B,N))
Nx =  round(Nx[0])
if A > 1.0:
    Ny = round(A*Nx) - 1.
elif A < 1.0:
    Ny = round(A*Nx) + 1
else:
    Ny = Nx
if B > 1.0:
    Nz = round(B*Nx) - 1.
elif B < 1.0:
    Nz = round(B*Nx) + 1
else:
    Nz = Nx

Nx = 10
Ny = 10
Nz = 4

print Nx, Ny, Nz

Ntrue = 2*Nx*Ny*Nz
vfTrue = Vv*Ntrue/VL                   

# One is lost in every other row
# another half is lost in each boundary row
Nx = Nx + 1 
Ny = Ny + 1  
Nz = Nz + 1

print vf
print vfTrue
print Ntrue

x0 = np.linspace(0.,Lx,Nx)     
y0 = np.linspace(0.,Ly,Ny)
z0 = np.linspace(0.,Lz,Nz)
dx = x0[1] - x0[0]
dy = y0[1] - y0[0]
dz = z0[1] - z0[0]

counter = 0

# for i in range(0,len(x0),2):
#     for j in range(len(y0)):
#         for k in range(len(z0)):
#             if  i %2 == 0 or j%2 == 0 or k % 2 == 0:
#                 x,y,z = sphere(x0[i],y0[j],z0[k],a0)
#                 ax.plot_surface(x, y, z, color="r")
#             else:
#             #if j < len(y0)-1 and i < len(x0)-1:
#                 x,y,z = sphere(x0[i]+dx/2.,y0[j]+dy/2.,z0[k]+dz/2,a0)
#                 ax.plot_surface(x, y, z, color="b")
#             #write to file
#             counter = counter  + 1
#             f.write('shap ' + str(counter) + ' {VOID}  1 \n') 
#             f.write('sphr ' + str(counter) + ' ' +  str(a0) + ' ' +  str(x0[i]) + \
#                     ' ' +  str(y0[j]) + ' ' +  str(z0[k])  + '\n')

#for i in range(0,len(x0),2):
#    for j in range(0,len(y0),2):
#        for k in range(0,len(z0),2):            
#            x,y,z = sphere(x0[i],y0[j],z0[k],a0)
#            ax.plot_surface(x, y, z, color="r")
#for i in range(1,len(x0),2):
#    for j in range(1,len(y0),2):
#        for k in range(1,len(z0),2):            
#            x,y,z = sphere(x0[i],y0[j],z0[k],a0)
#            ax.plot_surface(x, y, z, color="b")
#            f.write('shap ' + str(counter) + ' {VOID}  1 \n') 
#            f.write('sphr ' + str(counter) + ' ' +  str(a0) + ' ' +  str(x0[i]) + \
#                    ' ' +  str(y0[j]) + ' ' +  str(z0[k])  + '\n')

for i in range(len(x0)):
   for j in range(len(y0)):
       for k in range(len(z0)): 
           xGrid = x0[i]
           yGrid = y0[j]
           zGrid = z0[k]
           x,y,z = sphere(xGrid,yGrid,zGrid,a0)
           ax.plot_surface(x, y, z, color="r")
           #write to file
           counter = counter  + 1
           f.write('shap ' + str(counter) + ' {VOID}  1 \n') 
           f.write('sphr ' + str(counter) + ' ' +  str(a0) + ' ' +  str(xGrid) + \
                    ' ' +  str(yGrid) + ' ' +  str(zGrid)  + '\n')
for i in range(len(x0)-1):
   for j in range(len(y0)-1):
       for k in range(len(z0)-1):
           xGrid = x0[i]+0.5*dx
           yGrid = y0[j]+0.5*dy
           zGrid = z0[k]+0.5*dz
           x,y,z = sphere(xGrid,yGrid,zGrid,a0)
           ax.plot_surface(x, y, z, color="b")
           #write to file
           counter = counter  + 1
           f.write('shap ' + str(counter) + ' {VOID}  1 \n') 
           f.write('sphr ' + str(counter) + ' ' +  str(a0) + ' ' +  str(xGrid) + \
                    ' ' +  str(yGrid) + ' ' +  str(zGrid)  + '\n')
ax.view_init(90, 0)
plt.xlim([0,Lx])
plt.ylim([0,Ly])
plt.xlabel('x')
plt.ylabel('y')
f.write('END') 
f.close()
plt.show()
