import numpy as np

buffCols = 10000


## Create Output file
fileOutput = open('grains_L1_C001500_ff_000181MeshFine-grainCent.txt','w') 

elsetFile = 'neperElsets-apsFine.inp'
fileInput = open(elsetFile,'r')

numElsetFile = 'neperNumElsets-apsFine.inp'
numElsets = int(np.loadtxt(numElsetFile))

centFile = 'grains_L1_C001500_ff_000181MeshFine-ElemCent.inp'
elemCentXYZ = np.loadtxt(centFile,delimiter=',')

grain = -1
col = 0


elemMatrix = np.zeros((numElsets,buffCols))
elemMatrix[:,:] = np.nan
for line in fileInput:
    words = line.split()
    if words:
        if words[0].lower() == '*elset,':
            grain = grain + 1
            col = 0
        else:
            for w in words:
                if w[-1] == ',':
                    num = int(w[0:-1])
                else:
                    num = int(w)

                elemMatrix[grain,col] = num
                col = col + 1
poly = 120
ind = ~np.isnan(elemMatrix[poly-1,:])


centXYZ = np.zeros((numElsets,3))
for i in range(numElsets):
    ind = ~np.isnan(elemMatrix[i,:])
    elemInd = elemMatrix[i,ind].astype(int)
    if elemInd.size == 0:
        centXYZ[i,0] = np.nan
        centXYZ[i,1] = np.nan
        centXYZ[i,2] = np.nan
    else:
        x = elemCentXYZ[elemInd-1,1]
        y = elemCentXYZ[elemInd-1,2]
        z = elemCentXYZ[elemInd-1,3]
        centXYZ[i,0] = np.mean(x)
        centXYZ[i,1] = np.mean(y)
        centXYZ[i,2] = np.mean(z)
        fileOutput.write(str(centXYZ[i,0]) + ',' + str(centXYZ[i,1]) + ',' + str(centXYZ[i,2])+ '\n')

print(centXYZ[:,:])

fileOutput.close()
