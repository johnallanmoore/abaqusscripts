#!/usr/bin/python
import numpy as np


###### files ######
#.inp file name
fileName = 'fullInclusion'
# shuffled grain (ie elset) numbers
shuffleFile = 'shuffle100.inp'

##### user options #######
# element type
elementType = 'C3D8R'
# material name
materialNameMatrix    = 'Material-1'
materialNameInclusion = 'Material-2'
# section value
sectionValue = ''

# materials
Youngs1 = 200000.
Youngs2 = 2000000.
Poissons1 = 0.3
Poissons2 = 0.3

#number of grains
numElsets = 100

mu, sigma = Youngs1, 10000. # mean and standard deviation
s = np.random.normal(mu, sigma, numElsets)

## Create Output file
fileOutput = open(fileName + 'Mesh-LE.inp','w') 
shuffleValues = np.loadtxt(shuffleFile,dtype=int)
shuffleValues = np.append(shuffleValues,0)
Youngs1Rnd = s[shuffleValues]

################ Element sets for Each Material ###################  
writeSection = False
writeMaterial = False


fileInput = open(fileName +'.inp','r')
for line in fileInput:
    words = line.split()
    if words:
        if words[0] == '*Solid': 
            if writeSection == False:
                writeSection = True
                ###### write sections ###################
                fileOutput.write('*Solid Section, elset=INCLUSION, material=MATERIAL-2'+ '\n')
                fileOutput.write(sectionValue + ',' + '\n')
                fileOutput.write('** Section: Section-1' + '\n')
                for i in range(numElsets):
                    fileOutput.write('*Solid Section, elset=poly' + str(i+1) +  ', material=' + materialNameMatrix  + '-' + str(i+1) + '\n')
                    fileOutput.write(sectionValue + ',' + '\n')
        elif words[0] == '*End':
            if len(words) > 1:
                if (words[1] == 'Part'):
                    writeSection = False
        elif words[0] == '*Material,':
            ############################################
            ################ Materials ###################  
            ###########################################
            fileOutput.write("""** 
** MATERIALS
** 
""")
            for i in range(numElsets):
                fileOutput.write('*Material, name='+ materialNameMatrix + '-' + str(i+1) + '\n')
                fileOutput.write('*Elastic' + '\n')
                fileOutput.write(str(Youngs1Rnd[i]) + ',' + str(Poissons1) + '\n')
                fileOutput.write('** ----------------------------------------------' + '\n')

            fileOutput.write('*Material, name='+ materialNameInclusion  + '\n')
            fileOutput.write('*Elastic' + '\n')
            fileOutput.write(str(Youngs2) + ',' + str(Poissons2) + '\n')
            break
        
    if writeSection == False:
        fileOutput.write(line)

fileInput.close()
 



############################################
################ Step 1 ###################  
###########################################
fileOutput.write("""** 
** STEP: Step-1
** 
*Step, name=Step-1, nlgeom=NO
*Static
1., 1., 1e-05, 1.
** 
""")

########### Boundary Conditions #######
fileOutput.write("""** 
** BOUNDARY CONDITIONS
** 
** Name: BC-1 Type: Displacement/Rotation
*Boundary
X0, 1, 1
** Name: BC-2 Type: Displacement/Rotation
*Boundary
Y0, 2, 2
** Name: BC-4 Type: Displacement/Rotation
*Boundary
Y1, 2, 2, 0.01
** Name: BC-3 Type: Displacement/Rotation
*Boundary
Z0, 3, 3 \n""")

########### output requests #######
fileOutput.write("""** 
** OUTPUT REQUESTS
** 
*Restart, write, number interval=1, time marks=NO
** 
** FIELD OUTPUT: F-Output-1
** 
*Output, field, variable=PRESELECT
*Element Output
EVOL, 
** 
** HISTORY OUTPUT: H-Output-1
**  \n""")

## end step
fileOutput.write('*End Step')
fileOutput.close()
