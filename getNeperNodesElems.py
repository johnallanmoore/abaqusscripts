#!/usr/bin/python

import numpy as np

import sys

try:
    sampleNum = sys.argv[1] #  var1
    elemNum = sys.argv[2] #  var2
    if int(elemNum) == 40:
        neperInput = '/home/moorej/neperJobs/fipWTrans/fipWTransTess'+ str(sampleNum) +'Elem' + str(elemNum) + 'x.inp'
    else:
        neperInput = '/home/moorej/neperJobs/parallelFip/parallelFipTess'+ str(sampleNum) +'Elem' + str(elemNum) + 'x.inp'
    print neperInput
except:
    # neper input file
    #neperInput = 'cuPlateMesh.inp'
    #neperInput = 'impactor.inp'
    #neperInput = 'vfExampleElem1k.inp'
    #neperInput = 'vfExampleElem125.inp'
    #neperInput = 'vfExampleElem64k.inp'
    #neperInput = 'foamCellElem91k.inp'
    #neperInput = 'fipTransformElem64k.inp'
    #neperInput = '/home/moorej/neperJobs/fipTrans134.inp'
    #neperInput = '/home/moorej/neperJobs/fipTrans200.inp'
    #neperInput = '/home/moorej/neperJobs/fipTrans286.inp'
    #neperInput = 'finePlateMesh.inp'
    #neperInput = 'coarsePlateMesh.inp'
    #neperInput = 'parallelFipTess1Elem11x.inp'
    #neperInput = 'grains_L1_C001500_ff_000181Mesh2.inp'
    neperInput = 'grains_L1_C001500_ff_000181MeshUnits.inp'
extensionFlag = True
#extension = '-fip'
#extension = '-plate'
#extension = '-impactor'
#extension = '-pfip' # paralized fip mesh
#extension = 'fTrans134' # different number of grains
#extension = 'fTrans200' # different number of grains
#extension = 'fTrans286' # different number of grains
#extension = '-tfip' # 50 instances fip w tranformation
#extension = '-finePlate'
#extension = '-coarsePlate'
#extension = '-OneGrainPerElem'
extension = '-apsCoarse'
extension = '-apsCoarseUnits'

if extensionFlag == True:
    #abaqus output files
    nodeFile = 'neperNodes' + extension + '.inp' 
    elementFile = 'neperElements' + extension + '.inp'
    elsetFile = 'neperElsets' + extension + '.inp'
    nsetFile = 'neperNsets' + extension + '.inp'
    numNsetsFile = 'neperNumNsets' + extension + '.inp'
    numElsetsFile = 'neperNumElsets' + extension + '.inp'
    elemInGrainsFile = 'elemInGrains' + extension + '.inp'
else:
    #abaqus output files
    nodeFile = 'neperNodes.inp'
    elementFile = 'neperElements.inp'
    elsetFile = 'neperElsets.inp'
    nsetFile = 'neperNsets.inp'
    numNsetsFile = 'neperNumNsets.inp'
    numElsetsFile = 'neperNumElsets.inp'
    elemInGrainsFile = 'elemInGrains.inp'

# open elemInGrainsFile here some it deletes old stuff
felemInGrains = open(elemInGrainsFile,'w')

fileInput = open(neperInput,'r')

counter = 0
counterElset = 0
counterNset = 0
firstElset = True
firstNset = True
isElset = False
elsetElemTotal = 0
for line in fileInput:
    words = line.split()
    if counter < 40000000000:
        try:
            if f.closed == False:
                #f.write(line+'\n' )
                f.write(line )
                # count the number of element in a elset line
                if words:
                    if isElset == True:
                        numElemInGrain = numElemInGrain + len(words)
        except:
            pass
        # this checks if there is anything in the lines
        if words:
            if words[0] == '*Node':
                print 'its a node'
                f = open(nodeFile,'w') 
            elif words[0] == '*Element,':
                print 'its an element'
                f = open(elementFile,'w') 
            elif words[0] == '*Elset,':
                if firstElset == True:
                    print 'its the first element set'
                    f = open(elsetFile,'w')
                    isElset = True
                    numElemInGrain = 0
                    firstElset = False
                else:
                    f = open(elsetFile,'a')
                    isElset = True
                    numElemInGrain = 0

                # write the elset card
                # skip the carriage return and add some Abaaqus stuff
                # use this one if the elsets are define after the istance
                #f.write(line[0:-1] + ', internal, instance=Part-1-1' + '\n' )
                # use this one if the elsets are define in the part)
                f.write(line[0:-1]  + '\n' )
                counterElset = counterElset + 1

            elif words[0] == '*Nset,':
                if firstNset == True:
                    print 'its the first node set'
                    f = open(nsetFile,'w')
                    firstNset = False
                else:
                    f = open(nsetFile,'a')

                # write the nset card
                # skip the carriage return and add some Abaaqus stuff
                f.write(line[0:-1] + ', internal, instance=Part-1-1' + '\n' )
                counterNset = counterNset + 1
        else:
            try:
                f.close()
            except:
                pass
            if isElset == True:
                felemInGrains.write(str(counterElset) + ',' + str(numElemInGrain) +'\n' )
                #print 'elset:' +  str(counterElset) + ' elems:' +  str(numElemInGrain)
                elsetElemTotal = elsetElemTotal +  numElemInGrain
                isElset = False
    counter = counter + 1


# write number of nsets and elsets to files
print 'there are ' + str(counterNset) + ' node sets'
fnset = open(numNsetsFile,'w') 
fnset.write(str(counterNset))
fnset.close()

print 'there are ' + str(counterElset) + ' element sets'
felset = open(numElsetsFile,'w') 
felset.write(str(counterElset))
felset.close()

print 'there are ' + str(elsetElemTotal) + ' elements'

felemInGrains.close()
fileInput.close()

