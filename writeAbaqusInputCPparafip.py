#!/usr/bin/python
import numpy as np
from random import *

import sys

def getElsetsMat2(vf,numeElsets,elemInGrains):
    # determine volume fraction of each grain to vary volume fraction.
    volFrac = np.zeros(numElsets)
    for i in range(numElsets):
        elemInThisGrain = elemInGrains[i]
        volFrac[i] = elemInThisGrain[1]/numElem

        #shuffled volume fraction
        volFracShuffle = volFrac[shuffleElsets-1]

        vfTotal = 0.0
        for i in range(len(volFracShuffle)):
            if vfTotal >= vf:
                # length of vfShuffle to include in material 2
                mat2Ind = i
                break
            else:
                vfTotal = vfTotal + volFracShuffle[i]

    print 'volume fraction ' + str(vfTotal)
    elsetsMat2 = shuffleElsets[0:mat2Ind]
    elsetsMat2 = np.sort(elsetsMat2)
    return elsetsMat2

###### files ######
#.inp file name
fileName = 'paraFipMesh'


try:
    sampleNum = sys.argv[1] #  var1
except:
    pass

# node file name
nodeFile = 'neperNodes-pfip.inp'
# element file name
elementFile = 'neperElements-pfip.inp'
# elset file name (with cards)
elsetFile = 'neperElsets-pfip.inp'
# nset file name (with cards)
nsetFile = 'neperNsets-pfip.inp'
# number of elsets
numElsetsFile = 'neperNumElsets-pfip.inp'
# number of elements in each grain
elemInGrainsFile = 'elemInGrains-pfip.inp'
# shuffled grain (ie elset) numbers
shuffleFile = 'shuffle100.inp'

#Orientation file
orientFile = './parFipEulers/textureRnd118Euler_' + str(sampleNum) + '.txt'

##### user options #######
# element type
elementType = 'C3D8R'
# material name
materialName1 = 'Material-1'
materialName2 = 'Material-2'
# section value
sectionValue = ''
# volume fraction
vf = 0.6


# number of cycles (1 = 0.5 cycles, 2 = 1, etc)
numCycle = 7

# mean strain/displacemnt
dispMean = 0.0

# strain/displacment amplitude
dispAmp = 2.*0.005
dispMax = dispMean + 0.5*dispAmp
dispMin = dispMean - 0.5*dispAmp

#output frequency
freq1 = 5 # first cycle
freq2 = 5 # subsiquent cycles


## Create Output file
fileOutput = open(fileName + '.inp','w') 

## Open input files
nodes = np.loadtxt(nodeFile,delimiter=',')
elements = np.loadtxt(elementFile,delimiter=',',dtype=int)
elsetObject = open(elsetFile,'r')
nsetObject =  open(nsetFile,'r')
elsetText = elsetObject.read()
nsetText = nsetObject.read()
numElsets = np.loadtxt(numElsetsFile,dtype=int)
elemInGrains = np.loadtxt(elemInGrainsFile,delimiter=',')
shuffleElsets = np.loadtxt(shuffleFile,dtype=int)

orient = np.loadtxt(orientFile,delimiter=',')

#close up files where data is already extracted
elsetObject.close()
nsetObject.close()
############################################
################ Header ###################  
###########################################
fileOutput.write("""*Heading
*Preprint, echo=NO, model=NO, history=NO, contact=NO
""")

############################################
################## Part  ###################
############################################
fileOutput.write("""**
** PARTS
** \n""")
fileOutput.write('*Part, name=Part-1'+'\n' )
# NODES
fileOutput.write('*Node' +'\n' )
s = nodes.shape
for i in range(s[0]):
    for j in range(s[1]):
        if j != 0:
            fileOutput.write('\t'+',')
            fileOutput.write(str(nodes[i,j]))
        else:
            fileOutput.write(str(int(nodes[i,j])))
    fileOutput.write('\n')

# ELEMENTS
fileOutput.write('*Element, type=' + elementType + '\n' )
s = elements.shape
for i in range(s[0]):
    for j in range(s[1]):
        if j != 0:
            fileOutput.write('\t'+',')
        fileOutput.write(str(elements[i,j]))
    fileOutput.write('\n')

# All Element Set
fileOutput.write('*ELSET, elset=Set-All, generate' + '\n' )
fileOutput.write(str(min(elements[0:s[0],0]))+',')
fileOutput.write(str(max(elements[0:s[0],0]))+'\n')
# assuming no skipped numbers
numElem = float(max(elements[0:s[0],0]))

################ Grain Element Sets (in part) ###################  
fileOutput.write(elsetText + '\n')

################ Element sets for Each Material ###################  
elemMat1File = 'elemMat1.inp'
elemMat2File = 'elemMat2.inp'
firstElset = True
firstElsetMat1 = True
firstElsetMat2 = True
elsetsMat2 = getElsetsMat2(vf,numElsets,elemInGrains)
counterElset = 1
counterMat2 = 1
fileInput = open(elsetFile,'r')
for line in fileInput:
    words = line.split()
    try:
        if fMat1.closed == False:
            fMat1.write(line)
    except:
        pass
    try:
        if fMat2.closed == False:
            fMat2.write(line)
    except:
        pass
    if words:
        if words[0] == '*Elset,':
            if firstElset == True:
                # need a first mat1 and first mat2 elset
                if counterElset == elsetsMat2[counterMat2-1]:
                    fMat2 = open(elemMat2File,'w')
                    counterMat2 = counterMat2 + 1
                    firstElsetMat2 = False
                else:
                    print 'first elset'
                    fMat1 = open(elemMat1File,'w')
                    firstElsetMat1 = False
                firstElset = False
            else:
                if counterElset == elsetsMat2[counterMat2-1]:
                    if firstElsetMat2 == True:
                        fMat2 = open(elemMat2File,'w')
                        firstElsetMat2 = False
                    else:
                        fMat2 = open(elemMat2File,'a')
                    counterMat2 = counterMat2 + 1
                else:
                    if firstElsetMat1 == True:
                        fMat1 = open(elemMat1File,'w')
                        firstElsetMat1 = False
                    else:
                        fMat1 = open(elemMat1File,'a')

            if counterMat2 >= len(elsetsMat2):
                counterMat2 = len(elsetsMat2) - 1
            counterElset = counterElset + 1
    else:
        try:
            fMat1.close()
        except:
            pass
        try:
            fMat2.close()
        except:
            pass
fileInput.close()

# Section (Need to put elemet sets in part, and remove instance for name)
fileOutput.write('** Section: Section-1' + '\n')
for i in range(numElsets):
    fileOutput.write('*Solid Section, elset=poly' + str(i+1) +  ', material=' + materialName1  + '-' + str(i+1) + '\n')
    fileOutput.write(sectionValue + ',' + '\n')
############################################
################ Hourglass Stiffness ####### 
###########################################
    fileOutput.write('*Hourglass Stiffness' + '\n')
    fileOutput.write('10., , 0., 0.' + '\n')
fileOutput.write('*End Part')
############################################
################ Assembly ###################  
###########################################
fileOutput.write("""
**  
**
** ASSEMBLY
**
*Assembly, name=Assembly
**  
*Instance, name=Part-1-1, part=Part-1
*End Instance""")
fileOutput.write('\n')
######### The rest of this stuff will probably be cut and paste from abaqus
################ Element Sets (after instance) ###################
# open element sets created earlier
elsetObjectMat1 = open(elemMat1File,'r')
elsetObjectMat2 = open(elemMat2File,'r')
elsetMat1 = elsetObjectMat1.read()
elsetMat2 = elsetObjectMat2.read()
# write cards for each material
fileOutput.write('*ELSET, elset='+ materialName1 +', internal, instance=Part-1-1' + '\n')
fileOutput.write(elsetMat1 + '\n')
if len(elsetsMat2) > 0:
    fileOutput.write('*ELSET, elset='+ materialName2 +', internal, instance=Part-1-1' + '\n')
    fileOutput.write(elsetMat2 + '\n')
# write the card for each elset
# write each eset  
# close each files
#fileOutput.write(elsetText + '\n')
################ Node Sets ###################
s = nodes.shape
refNode = int(nodes[s[0]-1,0])
fileOutput.write('*Nset, nset=refNode, internal, instance=Part-1-1' + '\n')
fileOutput.write(str(refNode) + '\n') 
#fileOutput.write(nsetText + '\n')

fileInput = open(nsetFile,'r')
nodeRefFlag = 0
for line in fileInput:
    words = line.split()
    if words:
        if len(words) >= 2:
            if words[1] == 'nset=y1,' or words[1] == 'nset=z1,':
                nodeRefFlag = 1
    if nodeRefFlag == 1:
        for w in range(len(words)):
            if words[w] == str(refNode):
                nodeRefFlag = 0
                fileOutput.write('\n')
                print 'found refnode' + str(words[w])
            else:
                fileOutput.write(words[w] + ' ')
        fileOutput.write('\n')
    else:
        fileOutput.write(line + '\n')
fileInput.close()
fileOutput.write('\n')
################ Equations ###################
fileOutput.write('''** Constraint: Constraint-1
*Equation
2
y1, 2, 1.
refNode, 2, -1.
** 
** Constraint: Constraint-2
*Equation
2
z1, 3, 1.
refNode, 3, -1.
** \n''')
fileOutput.write('*End Assembly \n')
 
############################################
################ Materials ###################  
###########################################

# is there a way for the section properteis to give orientation?
# then I would only need 2 materials and it reads section prop
# use *ORIENTATION and ORIENT subroutine 
fileOutput.write("""** 
** MATERIALS
** 
""")

materialProps1 = '75000,0.3'
materialProps2 = '200000,0.33'

youngs1 = 75000.0

for i in range(numElsets):
    fileOutput.write('*Material, name='+ materialName1 + '-' + str(i+1) + '\n')
#     fileOutput.write('''*DEPVAR
# 74
# *USER MATERIAL, CONSTANTS=23
# 80869.,40356.,20257.,0.002,10.,320.0,0.0,0.0,
# 0.0, 1.0,0.0,500.0, 0.0,0.0, 2.0,''')
#     fileOutput.write(str(orient[i,0]) + ',' + '\n')
#     fileOutput.write(str(orient[i,1]) + ',' + str(orient[i,2]) + ',')
#     fileOutput.write('1.0, 1.0, 1.15, 3.0, 1.599')
#     fileOutput.write( '\n')
#     fileOutput.write('** ----------------------------------------------' + '\n')

    fileOutput.write('''*DEPVAR
74
*USER MATERIAL, CONSTANTS=25
162400.,92000.,69000.,180700.,46700.,0.001,50.,333.65,
0.0, 0.0, 0.0, 1.0, 0.0, 500.0, 0.0,0.0,'''+ '\n')
    fileOutput.write('2.0,')
    fileOutput.write(str(orient[i,0]) + ',')
    fileOutput.write(str(orient[i,1]) + ',' + str(orient[i,2]) + ',')
    fileOutput.write('1.0, 1.0, 1.15, 2.123,' + '\n')
    fileOutput.write( '1.599' )
    fileOutput.write( '\n')
    fileOutput.write('** ----------------------------------------------' + '\n')

############################################
################ Step 1 ###################  
###########################################
for s in range(numCycle):
    fileOutput.write('** \n')
    fileOutput.write('** STEP: Step-' + str(s+1)+'\n')
    fileOutput.write('** \n')
    fileOutput.write('*Step, name=Step-'+ str(s+1)+', nlgeom=YES, inc=100000 \n')
    fileOutput.write("""*Static
0.0625, 1., 1e-05, 1.
** """)

    ########### Boundary Conditions #######
    fileOutput.write("""
** 
** BOUNDARY CONDITIONS
** 
*BOUNDARY, type=DISPLACEMENT                          
x0, 1, 1 ,0.0 
*BOUNDARY, type=DISPLACEMENT \n""" )
    if s%2 == 0:
        disp = dispMax
    else:
        disp = dispMin                       
    fileOutput.write('x1, 1, 1 ,' + str(disp) + '\n')    
    fileOutput.write("""*BOUNDARY, type=DISPLACEMENT                          
y0, 2, 2 ,0.0 
*BOUNDARY, type=DISPLACEMENT                          
z0, 3, 3 ,0.0  \n""")

    ########### output requests #######
    fileOutput.write("""** 
** OUTPUT REQUESTS
** 
*Restart, write, number interval=1, time marks=NO
** 
** FIELD OUTPUT: F-Output-1
** \n""")
    if s == 0:
        fileOutput.write('*Output, field, variable=PRESELECT, FREQUENCY= ' + str(freq1) + '\n' )
    else:
        fileOutput.write('*Output, field, variable=PRESELECT, FREQUENCY= ' + str(freq2) + '\n' )
    fileOutput.write("""*Element Output
*Element Output
LE, PE, PEEQ, S,  SDV, EVOL
*Node Output
U, RF
** 
** HISTORY OUTPUT: H-Output-1
**  \n""")

    ## end step
    fileOutput.write('*End Step \n')
#elsetObject.close()
#nsetObject.close()
fileOutput.close()
