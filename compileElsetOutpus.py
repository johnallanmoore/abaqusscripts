import numpy as np
import os
#grainStart = 0
#grainEnd = 195

grainStart = 0
grainEnd = 195

outName = 'fineMeshReducedBase-Compiled--Elsets.inp'
outFile = open(outName,'w')
for i in range(grainStart,grainEnd+1):
    fileName ='fineMeshReducedBase-' + str(i) + '--Elsets.inp'
    fileInput = open(fileName,'r')
    if os.stat(fileName).st_size != 0:
        for line in fileInput:
            outFile.write(line)
    else:
        print('mssing file ' + fileName)
    fileInput.close()

outFile.close()
