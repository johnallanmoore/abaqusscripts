#!/usr/bin/python

import numpy as np
import datetime

from abaqusConstants import *
from abaqus import *

###############################
######   USER INPUTS  #########
###############################
# cae name without the .cae
deckName = 'wavySurfaceMesh'
deckName = 'wavySurfaceMesh2'
#deckName = 'S120-L1_ROTATED'

# name of part
partName = 'PART-1'

# node Set name
nsetName = 'topNset'

# Region to creat nset
xMin = 0.2
xMax = 0.3
yMin = 0.25
yMax =  10
zMin = 0.0001
zMax = 0.2499

# output files
nsetFile = deckName + '-Nsets.inp'
fileOutput = open(nsetFile ,'w')



# find surface nodes
mdb=openMdb( deckName + '.cae')



p = mdb.models[deckName].parts[partName]
surf_nodes = []

t1 = datetime.datetime.now()
for face in p.elementFaces:
    if len(face.getElements()) == 1:
        # Then the face has only one associated element, ie it's on the surface.
        # Get the nodes on the face:
        surf_nodes.extend([node for node in face.getNodes() if node not in surf_nodes])
#

numNodes = len(surf_nodes)

print 'Number of Surface Nodes: ' + str(numNodes)

nodeInd = np.empty((numNodes,))
nodeInd[:] = np.nan

print 'start searching nodes           : ' + str(datetime.datetime.now())

counter = 0
for i in range(numNodes):
    coords = surf_nodes[i].coordinates
    xVal = coords[0]
    yVal = coords[1]
    zVal = coords[2]
    if xMin <=  xVal and xVal <= xMax:
        if yMin <=  yVal and yVal <= yMax:
            if zMin <=  zVal and zVal <= zMax:
                nodeInd[counter] = int(surf_nodes[i].label)
                counter = counter + 1
nodeInd = nodeInd[~np.isnan(nodeInd)]

print 'end searching nodes             : ' + str(datetime.datetime.now())

print 'start writing nodes to file  : ' + str(datetime.datetime.now())
fileOutput.write('*Nset, nset=' + nsetName + '\n')

for i in range(len(nodeInd)):
    fileOutput.write(str(int(nodeInd[i])) + ', ')
    if i != 0 and i % 15 == 0:
        fileOutput.write('\n')        
print 'end writing nodes to file    : ' + str(datetime.datetime.now())                         

t2 = datetime.datetime.now()
print(t2 - t1)

fileOutput.close()

